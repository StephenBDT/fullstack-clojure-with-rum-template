# 1. Rename to your Project

Run the follwing from the root dir of this project to get the renaming:
```bash
NEW_NAME=myproj\
 && git mv src/clj/projectname src/clj/$NEW_NAME\
 && git mv src/cljs/projectname src/cljs/$NEW_NAME\
 && git mv src/cljc/projectname src/cljc/$NEW_NAME\
 && grep -rl projectname . | xargs sed -i "s/projectname/$NEW_NAME/g"
```

# 2. Start the Server
in one Terminal execute `clj -A:run` to start the clj server

# 3. Start the ClojureScript live reloading
in another terminal execute `clj -A:shadow-cljs watch projectname` which builds and watches the cljs

# You can also integrate the REPL:
To get REPL support,follow the comment instructions from your favorite clojure editor in `src/cljs/projectname/core.cljs`
