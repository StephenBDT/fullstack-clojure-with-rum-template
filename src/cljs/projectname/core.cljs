(ns projectname.core)

(defn ^:export init []
; (js/alert "Im Working") ;;
  (println "Hello World from cljs!")) ;; should be visible in js console in the browser

 ;; Start after cljs-jack-in:
   ;; shadow.user> (shadow/watch :projectname)
   ;; [:projectname] Configuring build.
   ;; [:projectname] Compiling ...
   ;; :watching[:projectname] Build completed. (114 files, 44 compiled, 0 warnings, 7.43s)
   ;; shadow.user> (shadow/repl :projectname)
   ;; cljs.user> (in-ns 'projectname.core)


