(ns projectname.core
  (:require [org.httpkit.server :refer [run-server]]
            [reitit.ring :as ring]
            [reitit.ring.middleware.exception :refer [exception-middleware]]
            [reitit.ring.middleware.muuntaja :refer [format-negotiate-middleware
                                                     format-request-middleware
                                                     format-response-middleware]]
            [rum.core :as rum]
            [muuntaja.core :as m]))


(rum/defc index [s]
  [:html
   [:body
    [:h1 s]
    [:div {:id "app"}]
    [:script {:src "/js/main.js"}]]])


(def index-html (rum/render-html (index "Hello World from clj")))


(def app
  (ring/ring-handler
   (ring/router
    [
     ["/" {:get (fn [arg] {:status 200, :body index-html})}]
     ["/api" {:get (fn [arg] {:status 200, :body {:hello "world"}})}]
     ]
    {:data {:muuntaja m/instance                          ;; This makes the /api return JSON
            :middleware [format-negotiate-middleware
                         format-request-middleware
                         exception-middleware
                         format-response-middleware]}})
   (ring/routes
    (ring/redirect-trailing-slash-handler)
    (ring/create-resource-handler {:path "/"})  ;; This servers the /js/main.js
                                                ;; (actually everything under resources/public)
    (ring/create-default-handler
     {:not-found (constantly {:status 404 :body "Route not found"})}))))


(defn -main []
  (run-server app {:port 4001})
  (println "Server started"))

